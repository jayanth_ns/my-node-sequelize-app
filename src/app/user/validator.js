import Joi from "joi";
import { sortByKeys } from "helpers/constants";
import { email, normalStr, validId } from "helpers/validators";

export default {
  // GET /api/samples/:id
  get: Joi.object({
    params: Joi.object({
      id: validId.required(),
    }),
  }),

  // GET /api/users
  list: Joi.object({
    query: Joi.object({
      sortBy: Joi.array().items(Joi.any().valid(sortByKeys)),
      limit: Joi.number().integer(),
      skip: Joi.number().integer(),
    }),
  }),

  // Register new user api
  // POST /api/users/new-user
  createUser: Joi.object({
    body: Joi.object({
      email: email.required(),
      password: normalStr.required(),
    }),
  }),

  // User login api
  // POST /api/users/login
  login: Joi.object({
    body: Joi.object({
      email: email.required(),
      password: normalStr.required(),
    }),
  }),

  // PUT /api/users/update/:id
  updateUser: Joi.object({
    params: Joi.object({
      id: validId.required(),
    }),
    body: Joi.object({
      email,
    }),
  }),

  // POST /api/users/deactivate/:id
  disableUser: Joi.object({
    params: Joi.object({
      id: validId.required(),
    }),
  }),
};
