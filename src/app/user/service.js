import models from "database";
import commonService from "helpers/services";

/**
 * Finding records with provided query params
 * @property {object} query - object containing params to prepare query.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {records[]}
 */
async function find({ query, autoFormat = true }) {
  const res = await commonService.find({
    Model: models.user,
    query,
    autoFormat,
  });
  return res;
}

/**
 * Finding record with id
 * @property {string} id - record id.
 * @property {string} errKey - key for which error object will be generated.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {record}
 */
async function findById({ id, errKey, autoFormat = true }) {
  const res = await commonService.findById({
    Model: models.user,
    id,
    errKey,
    autoFormat,
  });
  return res;
}

/**
 * Checking user record with email
 * @property {object} data - record properties.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {record}
 */
async function checkUserWithEmail({ email }) {
  const existingRec = await models.user.findOne({
    where: {
      email,
    },
  });
  if (existingRec !== null) {
    // Returning formatted response if autoFormat true
    return {
      error: {
        status: 409,
        message: "User with this email is already exists.",
      },
    };
  }
  return false;
}

/**
 * Checking user record with email
 * @property {string} email - record email.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {record}
 */
async function findUserByEmail({ email }) {
  const userRec = await models.user.findOne({
    where: {
      email,
    },
  });
  if (userRec) {
    return userRec;
  }
  return {
    error: {
      status: 401,
      message: "Invalid authentication credentials",
    },
  };
}

/**
 * Creating record
 * @property {object} data - record properties.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {record}
 */
async function create({ data, autoFormat = true }) {
  const res = await commonService.create({
    Model: models.user,
    data,
    autoFormat,
  });
  return res;
}

/**
 * Updating record
 * @property {object} data - record properties.
 * @property {record} existingRec - record which needs to be updated.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {record}
 */
async function updateExisting({ data, existingRec, autoFormat = true }) {
  const res = await commonService.updateExisting({
    data,
    existingRec,
    autoFormat,
  });
  return res;
}

/**
 * Updating record
 * @property {object} data - record properties.
 * @property {object} filterCriteria - criteria to fetch record to be updated.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {record}
 */
async function update({ data, filterCriteria, autoFormat = true }) {
  const res = await commonService.update({
    Model: models.user,
    data,
    filterCriteria,
    autoFormat,
  });
  return res;
}

/**
 * Pseudo delete record
 * @property {string} id - record id to be removed.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {record}
 */
async function disableById({ id, autoFormat = true }) {
  const res = await commonService.removeById({
    Model: models.user,
    id,
    autoFormat,
  });
  return res;
}
export default {
  find,
  findById,
  findUserByEmail,
  checkUserWithEmail,
  create,
  update,
  updateExisting,
  disableById,
};
