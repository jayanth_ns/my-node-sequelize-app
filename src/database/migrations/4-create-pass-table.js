// Importing initial schema
import userSchema from "src/app/user/models/user/schema";

// Actual pluralized table name in db
const dbTableName = "Passes";

/*
  to run migrations, follow this steps:
  1. export your NODE_ENV
  2. npm run sequelize db:migrate;
*/
export default {
  /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
  up: queryInterface => queryInterface.createTable(dbTableName, userSchema),
  /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  down: queryInterface => queryInterface.dropTable(dbTableName),
};
