import DataTypes from "sequelize";
// Importing common schema for all the table
import baseSchema from "src/database/baseSchema";

/*
  Defining initial schema in one file which can be
  imported in model and migration both
*/
export default {
  ...baseSchema,
  id: {
    autoIncrement: true,
    type: DataTypes.INTEGER,
    primaryKey: true,
    allowNull: false,
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true,
  },
  password: {
    type: DataTypes.STRING(64),
    allowNull: true,
  },
  forgotPasswordToken: {
    type: DataTypes.STRING(64),
    allowNull: true,
  },
  lastLogin: {
    type: DataTypes.DATE,
    allowNull: true,
  },
  isEmailVerified: {
    type: DataTypes.BOOLEAN,
    defaultValue: false,
  },
  isActive: {
    type: DataTypes.BOOLEAN,
    defaultValue: false,
  },
};
