import { FilterErrorAndThrow } from "helpers";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import service from "./service";
import validator from "./validator";
import config from "@config";

/**
 * Get record
 * @property {string} params.id - record Id.
 * @returns {Object}
 */
// eslint-disable-line
async function get(params) {
  // Validating param
  const validParam = await validator.get.validate({
    params,
  });

  const { id } = validParam.params;

  // Getting record details
  const existingRec = await service.findById({
    id,
  });

  // Throwing error if promise response has any error object
  FilterErrorAndThrow(existingRec);

  return existingRec;
}

/**
 * Get records list.
 * @property {number} query.skip - Number of records to be skipped.
 * @property {number} query.limit - Limit number of records to be returned.
 * @property {array} query.sortBy - keys to use to record sorting.
 * @returns {Object}
 */
async function list(query) {
  // Validating query
  const validQuery = await validator.list.validate({
    query,
  });
  // Getting record list with filters
  const records = await service.find({
    query: validQuery.query,
  });
  return records;
}

/**
 * Create new record

 * @property {string} body.email - The email of record.
 * @property {string} body.password - The password of record.
 * @returns {Object}
 */
async function createUser(body) {
  // Validating body
  const validData = await validator.createUser.validate({
    body,
  });

  // Hashing the password
  const hashPassword = await bcrypt.hash(validData.body.password, 10);
  const validBody = { ...validData.body, password: hashPassword };

  // TODO: Validate body, if anything needs to be checked with model

  // Creating new record
  const duplicateUser = await service.checkUserWithEmail({
    email: validBody.email,
  });

  // Throwing error if promise response has any error object
  FilterErrorAndThrow(duplicateUser);

  const newRec = await service.create({
    data: validBody,
  });

  // Throwing error if promise response has any error object
  FilterErrorAndThrow(newRec);

  return newRec;
}

/**
 * Login

 * @property {string} body.email - The email of record.
 * @property {string} body.password - The password of record.
 * @returns {Object}
 */
async function login(body) {
  // Validating body
  const validData = await validator.login.validate({
    body,
  });
  const userObj = await service.findUserByEmail({
    email: validData.body.email,
  });
  FilterErrorAndThrow(userObj);
  // checking the password
  const passwordMatched = await bcrypt.compare(
    validData.body.password,
    userObj.password,
  );
  if (!passwordMatched) {
    FilterErrorAndThrow({
      error: {
        status: 401,
        message: "Invalid authentication credentials",
      },
    });
  }
  // Creating JWT token for the user
  const payload = {
    email: userObj.email,
    id: userObj.id,
    username: userObj.username,
  };
  const expireObj = { expiresIn: "2h" };
  const token = await jwt.sign(payload, config.jwt.privateKey, expireObj);
  return {
    status: 200,
    data: {
      email: userObj.email,
      id: userObj.id,
      username: userObj.username,
      token,
    },
    message: "Successfully logged in",
  };
}

/**
 * Update existing record
 * @property {string} params.id - record Id.
 * @property {string} body.name - The name of record.
 * @property {string} body.email - The email of record.
 * @property {string} body.username - The username of record.
 * @returns {Object}
 */
async function update(params, body) {
  // Validating param
  const validParam = await validator.updateUser.validate({
    params,
  });

  const { id } = validParam.params;

  // Getting record object to be updated
  const existingRec = await service.findById({
    id,
    autoFormat: false,
  });

  // Throwing error if promise response has any error object
  FilterErrorAndThrow(existingRec);

  // Validating body
  const validData = await validator.update.validate({
    body,
  });

  const validBody = validData.body;

  // TODO: Validate body, if anything needs to be checked with model

  // Updating new data to record
  const savedRec = await service.updateExisting({
    existingRec,
    data: validBody,
  });

  // Throwing error if promise response has any error object
  FilterErrorAndThrow(savedRec);

  return savedRec;
}

/**
 * Delete record.
 * @property {string} params.id - record Id.
 * @returns {Object}
 */
async function disable(params) {
  // Validating param
  const validParam = await validator.disableUser.validate({
    params,
  });

  const { id } = validParam.params;

  // Updating status to deleted
  const disabledRec = await service.disableById({
    id,
  });

  // Throwing error if promise response has any error object
  FilterErrorAndThrow(disabledRec);

  return disabledRec;
}

export default {
  get,
  list,
  createUser,
  login,
  update,
  disable,
};
