import express from "express";
import sampleRoutes from "./src/app/sample/route";
import userRoutes from "./src/app/user/route";

const router = express.Router();

/** GET /health-check - Check service health */
router.get("/health-check", (req, res) => res.send("OK"));

// mount sample routes at /sample
router.use("/samples", sampleRoutes);

// ount user routes at /users
router.use("/users", userRoutes);
export default router;
